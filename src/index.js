import App from "./App";
import React from "react"
import ReactDOM from "react-dom"
import style from "./main.css";

ReactDOM.render( <App />, document.getElementById('app'))