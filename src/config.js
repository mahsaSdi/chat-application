const tokenUrl= "https://us1.pusherplatform.io/services/chatkit_token_provider/v1/908c8598-a0af-4015-94b9-1a5a833fce01/token";
const instanceLocator="v1:us1:908c8598-a0af-4015-94b9-1a5a833fce01";
// const tokenUrl = "https://us1.pusherplatform.io/services/chatkit_token_provider/v1/bed9cd0c-1420-4c23-a56f-4443b102ea1c/token";
// const instanceLocator = "v1:us1:bed9cd0c-1420-4c23-a56f-4443b102ea1c";
export {tokenUrl,instanceLocator}