import React from 'react'
import {ChatManager, TokenProvider} from '@pusher/chatkit-client'
import MessageList from './components/MessageList'
import SendMessageForm from './components/SendMessageForm'
import RoomList from './components/RoomList'
import UserList from './components/UserList'
import NewRoomForm from './components/NewRoomForm'
// import { ChatManager, TokenProvider } from '@pusher/chatkit-client'
import {tokenUrl,instanceLocator} from './config.js'

class App extends React.Component {
    state = {
        roomId: null,
        userId:"mahsa",
        messages:[],
        joinableRooms: [],
        joinedRooms: [],
        avatar:""
    }
    componentDidMount(){
        const chatManager = new ChatManager({
            instanceLocator,
            userId:'mahsa',
            tokenProvider: new TokenProvider({
                url:tokenUrl
            })
        })
        chatManager.connect()
        .then(currentUser =>{
            console.log('currentUser', currentUser)
            this.currentUser=currentUser;
            this.currentUser.avatarURL="https://api.adorable.io/avatars/141/abott@adorable.png"
            // console.log("users",this.currentUser.rooms[0].userStore);
            this.getAvatar();
            this.getRooms();
            // currentUser.subscribeToRoomMultipart({
            //     roomId,
            //     messageLimit:20,
            //     hooks:{
            //         onMessage: message =>{
            //             // console.log('message',message);
                       
            //             this.setState({
            //                 messages:[...this.state.messages, message]
            //             })
            //         }
            //     }
            // })
        })
       


    }
    getAvatar=()=>{
        if(this.currentUser)
        this.setState({
            avatar:this.currentUser.avatarURL
        })
    }
    getRooms=()=>{
        this.currentUser.getJoinableRooms()
        .then(joinableRooms => {
            this.setState({
                joinableRooms,
                joinedRooms:this.currentUser.rooms

            })
        })
        .catch(err => console.log('error on joined rooms if unload:',err))
    }
    subscribeToRoom=(roomId)=> {
        this.setState({ messages: [],roomId:roomId })
        this.currentUser.subscribeToRoomMultipart({
            roomId,
            hooks: {
                onMessage: message => {
                    this.setState({
                        messages: [...this.state.messages, message]
                    })
                },
                onUserStartedTyping: user => {
                    /** render out the users */
                }
            }
        })
        .then(room => {
            // this.setState({
            //     roomId: room.id
            // })
        /////////// when subscribed then getRooms execute////////////////////////////
            this.getRooms() 

        })
    }
    sendMessage = (text)=>{
        this.currentUser.sendMessage({
            text,
            roomId:this.state.roomId
        })
    }
    createRoom=(name)=> {
        this.currentUser.createRoom({
            name
        })
        .then(room => this.subscribeToRoom(room.id))
        .catch(err => console.log('error with createRoom: ', err))
    }
    render() {
        // console.log("aa",this.state.roomId);
        return (
            <div className="app">
                {/* <RoomList subscribeToRoom={this.subscribeToRoom} roomsjoine={[...this.state.joinableRooms]} roomsjoiened={[...this.state.joinedRooms]} idOfRoom={this.state.roomId} /> */}
                <RoomList subscribeToRoom={this.subscribeToRoom} roomsjoiened={[...this.state.joinedRooms, ...this.state.joinableRooms]} idOfRoom={this.state.roomId} />
                <MessageList userId={this.state.userId} avatar={this.state.avatar} roomId={this.state.roomId} message={this.state.messages} />
                <SendMessageForm disabled={!this.state.roomId} sendMessage={this.sendMessage} />
                <NewRoomForm  createRoom={this.createRoom}/>
                <UserList subscribeToRoom={this.subscribeToRoom} roomsjoiened={[...this.state.joinedRooms, ...this.state.joinableRooms]} idOfRoom={this.state.roomId} />
            </div>
        );
    }
}

export default App