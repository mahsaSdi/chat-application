import React from 'react'
import ReactDOM from "react-dom"

class UserList extends React.Component {
    componentWillUpdate() {
        const node = ReactDOM.findDOMNode(this)
        //scrollTop is the distance from the top. clientHeight is the visible height. scrollHeight is the height on the component
        this.shouldScrollToBottom = node.scrollTop + node.clientHeight + 100 >= node.scrollHeight
    }
    componentDidUpdate() {
        //scroll to the bottom if we are close to the bottom of the component
        if (this.shouldScrollToBottom) {
            const node = ReactDOM.findDOMNode(this)
            node.scrollTop = node.scrollHeight
        }
    }

    render() {
        let sortedUser;
        this.props.roomsjoiened.sort((a, b) => (a.name.toUpperCase() > b.name.toUpperCase()) ? 1 : -1).map(room => {
            let users;
            if (room.id == this.props.idOfRoom) {
                users = room.userIds;
                sortedUser = users.map((eachUser, index) => {
                let position = room.userStore.presenceStore[eachUser];
                    return (
                        // show each user and position
                        <li key={index} className="showUsers">
                            <div >
                                <span>{eachUser}</span>
                                <span className="alignePosition">{position}</span>
                            </div>
                        </li>
                    )
                })
            }
        })
        return (
            <div className="new-user-list">
                <ul>
                    <h3>users:</h3>
                    {sortedUser}
                </ul>
            </div>
        )
    }
}

export default UserList