import React from 'react'
import ReactDOM from "react-dom"

class RoomList extends React.Component {
    componentWillUpdate() {
        const node = ReactDOM.findDOMNode(this)
        //scrollTop is the distance from the top. clientHeight is the visible height. scrollHeight is the height on the component
        this.shouldScrollToBottom = node.scrollTop + node.clientHeight + 100 >= node.scrollHeight
    }
    componentDidUpdate() {
        //scroll to the bottom if we are close to the bottom of the component
        if (this.shouldScrollToBottom) {
            const node = ReactDOM.findDOMNode(this)
            node.scrollTop = node.scrollHeight
        }
    }
    render() {
        return (
            <div className="rooms-list">
                <ul>
                    <h3 style={{ color: "white" }}>Your rooms:</h3>
                    {this.props.roomsjoiened.sort((a, b) => (a.name.toUpperCase() > b.name.toUpperCase()) ? 1 : -1).map(room => {

                        return (
                            <li key={room.id} className="roomplus" style={{ color: room.id == this.props.idOfRoom ? "white" : "black", cursor: 'pointer' }}>
                                <div
                                    onClick={() => this.props.subscribeToRoom(room.id)}>
                                    {room.name}
                                </div>
                            </li>
                        )
                    })}
                </ul>
            </div>
        )
    }
}

export default RoomList