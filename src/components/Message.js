import React from 'react'

const Message = props => {
    let user;
    if (props.eachsender == props.userId) {
        user = <div className="message-username"><span><img src={props.avatar} alt="Avatar" className="avatar"></img>{" " + props.eachsender}</span></div>
    } else {
        user = <div className="message-username">{"             " + props.eachsender}</div>
    }
    return (
        <div className="message">
            <div className="message-username">  {user}</div>
            <div className="message-text">{props.eachmessage}</div>
        </div>
    )

}

export default Message
