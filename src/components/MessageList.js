import React from 'react'
import Message from './Message.js';
import ReactDOM from "react-dom"

class MessageList extends React.Component {
    componentWillUpdate() {
        const node = ReactDOM.findDOMNode(this)
        //scrollTop is the distance from the top. clientHeight is the visible height. scrollHeight is the height on the component
        this.shouldScrollToBottom = node.scrollTop + node.clientHeight + 100 >= node.scrollHeight
    }
    componentDidUpdate() {
        //scroll to the bottom if we are close to the bottom of the component
        if (this.shouldScrollToBottom) {
            const node = ReactDOM.findDOMNode(this)
            node.scrollTop = node.scrollHeight
        }
    }

    render() {
        if (!this.props.roomId) {
            return (
                <div className="message-list">
                    <div className="join-room">
                        &larr; Join a room!
                    </div>
                </div>
            )
        }

        return (
            <div className="message-list" id="list">
                <div>{
                    this.props.message.map(message => {
                        // this let variable change username with name
                        let name = message.userStore.users[message.senderId].name;
                        return (
                            <div key={message.id} className="message">
                                <Message avatar={this.props.avatar} userId={this.props.userId} eachsender={name} eachmessage={message.parts[0].payload.content} />
                            </div>
                        )
                    })
                }</div>
            </div>
        )
    }
}

export default MessageList